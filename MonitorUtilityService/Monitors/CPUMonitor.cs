using System.Diagnostics;
using System.Linq;

namespace MonitorUtility.Monitors
{
    // ReSharper disable once InconsistentNaming
    internal class CPUMonitor : AbstractMonitor
    {
        private PerformanceCounter _cpuCounter;
        private FixedSizedQueue<float> _cpuTimeQueue;
        public override bool Enabled { get; set; }
        public override bool Dominant { get; set; }
        public bool Inversive { get; set; }
        // ReSharper disable InconsistentNaming
        public double CPUThresholdPercentage { get; set; }
        // ReSharper restore InconsistentNaming
        public override void ValidateMonitorSettings(MonitoringProfile settings)
        {
            CPUThresholdPercentage = Utils.Clamp(CPUThresholdPercentage, MinCPUThresholdPercentage,
                MaxCPUThresholdPercentage);
        }

        public override void Init(MonitoringProfile settings)
        {
            _cpuTimeQueue = new FixedSizedQueue<float> {Limit = settings.MonitoringTimeInMS/settings.CheckIntervallInMS};

            _cpuCounter = new PerformanceCounter
            {
                CategoryName = "Processor",
                CounterName = "% Processor Time",
                InstanceName = "_Total"
            };

            Log($"Queue: {_cpuTimeQueue}, CPU Counter: {_cpuCounter}");
        }

        public override void IterateMonitoringStep(MonitoringProfile settings)
        {
            _cpuTimeQueue.Enqueue(_cpuCounter.NextValue());
            Log($"Queue: {_cpuTimeQueue}");
        }

        public override bool IsContitionMet(MonitoringProfile settings)
        {
            var result = _cpuTimeQueue.Count == _cpuTimeQueue.Limit && _cpuTimeQueue.All(it => (Inversive
                ? it >= CPUThresholdPercentage
                : it < CPUThresholdPercentage));
            Log(result);
            return result;
        }

        public override void ResetMonitor(MonitoringProfile settings)
        {
            _cpuTimeQueue.Clear();
            Log($"Queue: {_cpuTimeQueue}");
        }

        // ReSharper disable InconsistentNaming
        public const double DefaultCPUThresholdPercentage = 10.0;
        public const double MinCPUThresholdPercentage = 0.5;
        public const double MaxCPUThresholdPercentage = 100.0;
        // ReSharper restore InconsistentNaming
    }
}