using System.Linq;
using NAudio.CoreAudioApi;

namespace MonitorUtility.Monitors
{
    internal class AudioMonitor : AbstractMonitor
    {
        public const double DefaultAudioThreshold = 0.1;
        public const double MinAudioLevelThreshold = 0.01;
        public const double MaxAudioLevelThreshold = 1.0;
        private FixedSizedQueue<float> _masterVolumeQueue;
        public override bool Enabled { get; set; }
        public override bool Dominant { get; set; }
        public double AudioLevelThreshold { get; set; }
        public bool Inversive { get; set; }

        public override void Init(MonitoringProfile settings)
        {
            _masterVolumeQueue = new FixedSizedQueue<float>
            {
                Limit = settings.MonitoringTimeInMS/settings.CheckIntervallInMS
            };
            Log($"Queue: {_masterVolumeQueue}");
        }

        public override void ValidateMonitorSettings(MonitoringProfile settings)
        {
            var before = AudioLevelThreshold;
            AudioLevelThreshold = Utils.Clamp(AudioLevelThreshold, MinAudioLevelThreshold, MaxAudioLevelThreshold);
            Log($"before AudioLevelThreshold: {before}, after AudioLevelThreshold: {AudioLevelThreshold}");
        }

        public override void IterateMonitoringStep(MonitoringProfile settings)
        {
            _masterVolumeQueue.Enqueue(GetMasterVolume());
            Log($"Queue: {_masterVolumeQueue}");
        }

        public override bool IsContitionMet(MonitoringProfile settings)
        {
            var result = _masterVolumeQueue.Count == _masterVolumeQueue.Limit &&
                         _masterVolumeQueue.All(it => (Inversive ? it >= AudioLevelThreshold : it < AudioLevelThreshold));
            Log(result);
            return result;
        }

        public override void ResetMonitor(MonitoringProfile settings)
        {
            _masterVolumeQueue.Clear();
            Log($"Queue: {_masterVolumeQueue}");
        }

        private static float GetMasterVolume()
        {
            var devEnum = new MMDeviceEnumerator();
            var defaultDevice = devEnum.GetDefaultAudioEndpoint(DataFlow.Render, Role.Multimedia);
            return defaultDevice.AudioMeterInformation.MasterPeakValue;
        }
    }
}