﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace MonitorUtility.Monitors
{
    internal abstract class AbstractMonitor : IMonitorable
    {
        public abstract bool Enabled { get; set; }
        public abstract bool Dominant { get; set; }
        public EventLog EventLog { get; set; }
        public abstract void Init(MonitoringProfile settings);
        public abstract void ValidateMonitorSettings(MonitoringProfile settings);
        public abstract void IterateMonitoringStep(MonitoringProfile settings);
        public abstract bool IsContitionMet(MonitoringProfile settings);
        public abstract void ResetMonitor(MonitoringProfile settings);

        public void Log(object message, EventLogEntryType logEntryType = EventLogEntryType.Information, [CallerMemberName] string callingMethod = null)
        {
            EventLog?.WriteEntry($"{GetType().Name} | {callingMethod} | {message ?? string.Empty}", logEntryType);
        }
    }
}