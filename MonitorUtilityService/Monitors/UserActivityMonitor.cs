﻿namespace MonitorUtility.Monitors
{
    internal class UserActivityMonitor : AbstractMonitor
    {
        // ReSharper disable InconsistentNaming
        public const int MaxIdleTimeInMS = 1000 * 60 * 60 * 24; // == 1 day
        public const int DefaultIdleTimeInMS = 1000 * 60 * 5; // 5 minutes

        private long _idleTimeinMS;
        public override bool Enabled { get; set; }
        public override bool Dominant { get; set; }

        public int IdleTimeInMS { get; set; }


        public override void Init(MonitoringProfile settings)
        {            _idleTimeinMS = 0;
            Log($"IdleTimeInMS: {_idleTimeinMS}");
        }

        public override void ValidateMonitorSettings(MonitoringProfile settings)
        {
            var before = IdleTimeInMS;
            IdleTimeInMS = Utils.Clamp(IdleTimeInMS, settings.CheckIntervallInMS, MaxIdleTimeInMS);
            Log($"before IdleTimeInMSThreshold: {before}, after IdleTimeInMSThreshold: {IdleTimeInMS}");

        }

        public override void IterateMonitoringStep(MonitoringProfile settings)
        {
            _idleTimeinMS = Win32.GetIdleTimeInMS();
            Log($"IdleTimeInMS: {_idleTimeinMS}");
        }

        public override bool IsContitionMet(MonitoringProfile settings)
        {
            var result = _idleTimeinMS >= IdleTimeInMS;
            Log(result);
            return result;
        }

        public override void ResetMonitor(MonitoringProfile settings)
        {
            //TODO: some useful reset of this condition
        }
    }
}