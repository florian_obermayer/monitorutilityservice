﻿using System.Diagnostics;
using Newtonsoft.Json;

namespace MonitorUtility
{
    public interface IMonitorable
    {
        bool Enabled { get; set; }

        bool Dominant { get; set; }
        [JsonIgnore]
        EventLog EventLog { get; set; }

        void Init(MonitoringProfile settings);
        void ValidateMonitorSettings(MonitoringProfile settings);
        void IterateMonitoringStep(MonitoringProfile settings);

        bool IsContitionMet(MonitoringProfile settings);

        void ResetMonitor(MonitoringProfile settings);

        void Log(object message, EventLogEntryType logEntryType = EventLogEntryType.Information,  string callingMethod = null);
    }
}