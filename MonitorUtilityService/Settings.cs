using System;
using System.IO;
using Newtonsoft.Json;

namespace MonitorUtility
{
    public class Settings
    {
        private const string FileName = "settings.json";
        private static Settings _instance;
        private readonly JsonSerializerSettings _jsonSerializerSettings;
        private readonly string _path;

        private Settings()
        {
            _jsonSerializerSettings = new JsonSerializerSettings
            {
                MissingMemberHandling = MissingMemberHandling.Ignore,
                Formatting = Formatting.Indented,
                TypeNameHandling = TypeNameHandling.Auto,
                TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple
            };
            
            var directory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                MonitorUtility.CurrentServiceName);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            _path = Path.Combine(directory, FileName);
            Values = LoadSettings();
            Store();
        }

        public SettingsJson Values { get; private set; }
        public static Settings Instance => _instance ?? (_instance = new Settings());

        private SettingsJson LoadSettings()
        {
            if (!File.Exists(_path))
            {
                Store();
            }
            return JsonConvert.DeserializeObject<SettingsJson>(
                File.ReadAllText(_path), _jsonSerializerSettings) ?? SettingsJson.DefaultSettings;
        }

        public void Store()
        {
            File.WriteAllText(_path, JsonConvert.SerializeObject(Values, _jsonSerializerSettings));
        }

        public void Reset()
        {
            Values = new SettingsJson();
            Store();
        }
    }
}