﻿using System.Threading;

namespace MonitorUtility
{
    public class ThreadParameter
    {
        public ManualResetEvent Semaphore { get; set; }
        public MonitoringProfile Profile { get; set; }
    }
}