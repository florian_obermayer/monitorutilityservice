﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace MonitorUtility
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
        

        private void ProjectInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            var controller = new ServiceController(MonitorUtilityService.ServiceName);
            if (controller.Status == ServiceControllerStatus.Stopped) {
                controller.Start();
            }
        }

        private void ProjectInstaller_BeforeUninstall(object sender, InstallEventArgs e)
        {
            var controller = new ServiceController(MonitorUtilityService.ServiceName);
            try
            {
                controller.Stop();
                controller.Close();

            }
            catch
            {
               // ignored
            }
        }
    }
}
