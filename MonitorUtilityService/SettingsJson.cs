﻿using System.Collections.Generic;
using System.Diagnostics;
using MonitorUtility.Monitors;
using Newtonsoft.Json;

// ReSharper disable InconsistentNaming

namespace MonitorUtility
{
    public class SettingsJson
    {
        public List<MonitoringProfile> MonitoringProfiles { get; set; }

        public static SettingsJson DefaultSettings => new SettingsJson
        {
            MonitoringProfiles = new List<MonitoringProfile>
            {
                new MonitoringProfile
                {
                    ProfileName = MonitoringProfile.DefaultProfileName,
                    Enabled = true,
                    MonitoringTimeInMS = MonitoringProfile.DefaultMonitoringTimeInMS,
                    CheckIntervallInMS = MonitoringProfile.DefaultCheckIntervallInMS,
                    Monitors = new List<IMonitorable>
                    {
                        new CPUMonitor
                        {CPUThresholdPercentage = CPUMonitor.DefaultCPUThresholdPercentage, Enabled = true},
                        new AudioMonitor {AudioLevelThreshold = AudioMonitor.DefaultAudioThreshold, Enabled = true},
                        new UserActivityMonitor {IdleTimeInMS = UserActivityMonitor.DefaultIdleTimeInMS, Enabled = true}
                    },
                    Processes = new List<ProcessInfo>
                    {
                        new ProcessInfo
                        {
                            ExecutablePath = ProcessInfo.DefaultExecutablePath,
                            Arguments = ProcessInfo.DefaultArguments
                        }
                    }
                }
            }
        };
    }

    public class ProcessInfo
    {
        
        public const string DefaultExecutablePath = "shutdown.exe";
        public const string DefaultArguments = "-s -t 30 -c \"Idle Detected\"";
        public string ExecutablePath { get; set; }
        public string Arguments { get; set; }
        public bool Blocking { get; set; }
        public int WaitTimeOut { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Domain { get; set; }
        public bool TryGetCurrenUserLoginNonBlocking { get; set; }
        public override string ToString()
        {
            return DefaultExecutablePath ?? base.ToString();
        }
    }

    public class MonitoringProfile
    {
        public const int MinCheckIntervallInMS = 100; // 100 ms
        public const int MaxCheckIntervallInMS = 1000*60; // 1 min
        public const int DefaultMonitoringTimeInMS = 1000*60*5; // 5 min
        public const int DefaultCheckIntervallInMS = 1000*2; // 2 sec
        public const int MaxMonitoringTimeInMS = 1000*60*60*24; // 1 day
        public const string DefaultProfileName = "DefaultProfile";
        public string ProfileName { get; set; }
        public bool Enabled { get; set; }
        public bool StopAfterProcessing { get; set; }
        public List<IMonitorable> Monitors { get; set; }
        public int MonitoringTimeInMS { get; set; }
        public int CheckIntervallInMS { get; set; }
        public List<ProcessInfo> Processes { get; set; }

        [JsonIgnore]
        public EventLog EventLog { get; set; }

        public override string ToString()
        {
            return ProfileName ?? base.ToString();
        }
    }
}