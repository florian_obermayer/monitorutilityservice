﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Windows;
using Win32;

namespace MonitorUtility
{
    public partial class MonitorUtility : ServiceBase
    {
        public const string CurrentServiceName = "MonitorUtilityService";
        private List<KeyValuePair<Thread, ThreadParameter>> _threadListWithParams;

        public MonitorUtility()
        {
            InitializeComponent();
            ServiceName = CurrentServiceName;
            CanStop = true;
            //Setup logging
            AutoLog = false;

            EventLog.BeginInit();
            if (!EventLog.SourceExists(ServiceName))
            {
                EventLog.CreateEventSource(ServiceName, "Application");
            }

            EventLog.EndInit();
            EventLog.Source = ServiceName;
            EventLog.Log = "Application";
        }

        protected override void OnStart(string[] args)
        {
            EventLog.WriteEntry("On start called..", EventLogEntryType.Information);

            CreateAndValidateGlobalSettings();


            var activeProfiles = Settings.Instance.Values.MonitoringProfiles.Where(p => p.Enabled).ToList();

            if (!activeProfiles.Any())
            {
                EventLog.WriteEntry("No profiles active. Service is stopping.", EventLogEntryType.Warning);
                Stop();
                return;
            }

            var activeProfilesWithActiveMonitors = activeProfiles.Where(p => p.Monitors.Any(m => m.Enabled)).ToList();

            if (!activeProfilesWithActiveMonitors.Any())
            {
                EventLog.WriteEntry("No monitors active. Service is stopping.", EventLogEntryType.Warning);
                Stop();
                return;
            }

            // enable monitor logging
#if DEBUG
            if(true)
#else
            if (args.Any( it => new[] {"d", "-d", "/d", "debug", "--debug" }.Contains(it.ToLower())))
#endif
            {
                foreach (var t in activeProfilesWithActiveMonitors)
                {
                    foreach (var monitor in t.Monitors)
                    {
                        monitor.EventLog = EventLog;
                    }
                }
            }

            _threadListWithParams = new List<KeyValuePair<Thread, ThreadParameter>>();


            foreach (var profileWithActiveMonitor in activeProfilesWithActiveMonitors)
            {
                // add threads with Threadparameters (Profile + Semaphore)
                _threadListWithParams.Add(new KeyValuePair<Thread, ThreadParameter>(new Thread(ThreadRunner)
                {
                    IsBackground = true,
                    Name = $"{profileWithActiveMonitor.ProfileName ?? "Unknown"} RunnerThread"
                }, new ThreadParameter {Profile = profileWithActiveMonitor, Semaphore = new ManualResetEvent(false)}));


                //initialize active monitors in profiles
                foreach (var activeMointor in profileWithActiveMonitor.Monitors.Where(it => it.Enabled))
                {
                    EventLog.WriteEntry($"Validating Monitor settings: {activeMointor.GetType().Name}",
                        EventLogEntryType.Information);
                    activeMointor.ValidateMonitorSettings(profileWithActiveMonitor);
                    EventLog.WriteEntry($"Init Monitor: {activeMointor.GetType().Name}", EventLogEntryType.Information);
                    activeMointor.Init(profileWithActiveMonitor);
                }
            }

            EventLog.WriteEntry(
                $"Starting worker threads: [ {string.Join(" | ", _threadListWithParams.Select(it => it.Key.Name))} ]",
                EventLogEntryType.Information);
            //start all worker threads
            foreach (var threadWithProfile in _threadListWithParams)
            {
                threadWithProfile.Key.Start(threadWithProfile.Value);
            }
        }

        private void ThreadRunner(object obj)
        {
            var param = obj as ThreadParameter;
            if (param == null)
            {
                EventLog.WriteEntry("Unknown ThreadRunner object", EventLogEntryType.Error);
                return;
            }

            var profile = param.Profile;
            var semaphore = param.Semaphore;

            var activeMonitors = profile.Monitors.Where(it => it.Enabled).ToList();

            while (!semaphore.WaitOne(0))
            {
                // process one monitoring iteration per monitor
                activeMonitors.ForEach(it => it.IterateMonitoringStep(profile));

                var dominantMonitors = activeMonitors.Where(it => it.Dominant && it.IsContitionMet(profile)).ToList();
                var nonDominantMonitors = activeMonitors.Where(it => !it.Dominant).ToList();

                // check if all monitors have met condition
                if (nonDominantMonitors.All(it => it.IsContitionMet(profile)) || dominantMonitors.Any())
                {
                    EventLog.WriteEntry($"Monitors for profile {profile.ProfileName} met condition, running processes",
                        EventLogEntryType.Information);
                    RunProcesses(profile);

                    EventLog.WriteEntry("Resetting affected monitors", EventLogEntryType.Information);
                    //reset all affected monitors
                    activeMonitors.Where(it => it.IsContitionMet(profile))
                        .ToList()
                        .ForEach(it => it.ResetMonitor(profile));

                    // stop profile when processes ran
                    if (profile.StopAfterProcessing)
                    {
                        EventLog.WriteEntry("Stop after processing enabled, service is will stop",
                            EventLogEntryType.Information);
                        semaphore.Set();
                        return;
                    }
                }

                Thread.Sleep(profile.CheckIntervallInMS);
            }
        }

        private void RunProcesses(MonitoringProfile profile)
        {
            foreach (var processInfo in profile.Processes)
            {
                EventLog.WriteEntry(
                    $"running process [{processInfo.ExecutablePath}] for profile {profile.ProfileName}",
                    EventLogEntryType.Information);
                if (processInfo.TryGetCurrenUserLoginNonBlocking)
                {
                    if (!TryGetProcessAsCurrentUser(processInfo.ExecutablePath, processInfo.Arguments))
                    {
                        EventLog.WriteEntry(
                            $"Couldn't call even from current user: {Win32API.GetLoggedInUserName() ?? "Unknown User"}. Running process as default user...",
                            EventLogEntryType.Warning);
                    }
                }
                RunProcessIndividual(processInfo);
            }
        }

        private void RunProcessIndividual(ProcessInfo info)
        {
            Process process;

            var isBlockingWithTimeout = info.Blocking && info.WaitTimeOut > 0;
            var isEndlessBlocking = info.Blocking && info.WaitTimeOut <= 0;

            var hasArguments = info.Arguments != null;
            var hasCredentials = info.Username != null;
            var hasPassword = info.Password != null;

            if (hasArguments)
            {
                if (hasCredentials)
                {
                    if (hasPassword)
                    {
                        var secureString = new SecureString();
                        foreach (var c in info.Password)
                        {
                            secureString.AppendChar(c);
                        }
                        process = Process.Start(info.ExecutablePath, info.Arguments, info.Username, secureString,
                            info.Domain);
                    }
                    else
                    {
                        process = Process.Start(info.ExecutablePath, info.Arguments, info.Username, null, info.Domain);
                    }
                }
                else
                {
                    process = Process.Start(info.ExecutablePath, info.Arguments);
                }
            }
            else
            {
                if (hasCredentials)
                {
                    if (hasPassword)
                    {
                        var secureString = new SecureString();
                        foreach (var c in info.Password)
                        {
                            secureString.AppendChar(c);
                        }
                        process = Process.Start(info.ExecutablePath, info.Username, secureString, info.Domain);
                    }
                    else
                    {
                        process = Process.Start(info.ExecutablePath, info.Username, null, info.Domain);
                    }
                }
                else
                {
                    process = Process.Start(info.ExecutablePath);
                }
            }

            if (isBlockingWithTimeout)
            {
                process?.WaitForExit(info.WaitTimeOut);
            }
            else if (isEndlessBlocking)
            {
                process?.WaitForExit();
            }
        }

        private bool TryGetProcessAsCurrentUser(string executablePath, string arguments)
        {
            var cmd = executablePath;
            if (!string.IsNullOrWhiteSpace(arguments))
            {
                cmd += $" {arguments}";
            }
            var output = new StringBuilder();
            var result = Win32API.CreateProcessAsUser(cmd, Environment.CurrentDirectory, Win32API.GetLoggedInUserName(),
                out output);
            if (output.Length != 0)
                EventLog.WriteEntry(output.ToString());
            return result;
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("OnStop called...", EventLogEntryType.Information);
            if (_threadListWithParams == null)
            {
                return;
            }

            foreach (var threadWithParams in _threadListWithParams)
            {
                threadWithParams.Value.Semaphore.Set();
            }

            foreach (var thread in _threadListWithParams.Select((pair, i) => pair.Key))
            {
                if (thread.IsAlive)
                {
                    EventLog.WriteEntry($"Waiting for thread [{thread.Name}]..", EventLogEntryType.Information);
                    if (!thread.Join(MonitoringProfile.DefaultCheckIntervallInMS))
                    {
                        EventLog.WriteEntry($"Thread [{thread.Name}] timed out. aborting thread...",
                            EventLogEntryType.Warning);
                        thread.Abort();
                    }
                }
            }

            EventLog.WriteEntry("OnStop completed.", EventLogEntryType.Information);
        }

        private void CreateAndValidateGlobalSettings()
        {
            EventLog.WriteEntry("creating settings", EventLogEntryType.Information);
            var settings = Settings.Instance.Values;
            EventLog.WriteEntry("checking settings", EventLogEntryType.Information);


            // generate profile if not existing
            if (settings.MonitoringProfiles == null || !settings.MonitoringProfiles.Any())
            {
                EventLog.WriteEntry("No monitoring profiles defined. Loading defaults", EventLogEntryType.Warning);
                settings.MonitoringProfiles =
                    new List<MonitoringProfile>(SettingsJson.DefaultSettings.MonitoringProfiles);
            }

            foreach (var profile in settings.MonitoringProfiles)
            {
                // check monitors of profiles 
                if (profile.Monitors == null || !profile.Monitors.Any())
                {
                    EventLog.WriteEntry("No monitors defined. Loading defaults", EventLogEntryType.Warning);
                    profile.Monitors =
                        new List<IMonitorable>(SettingsJson.DefaultSettings.MonitoringProfiles.First().Monitors);
                }
                profile.CheckIntervallInMS = Utils.Clamp(profile.CheckIntervallInMS,
                    MonitoringProfile.MinCheckIntervallInMS, MonitoringProfile.MaxCheckIntervallInMS);
                profile.MonitoringTimeInMS = Utils.Clamp(profile.MonitoringTimeInMS, profile.CheckIntervallInMS,
                    MonitoringProfile.MaxMonitoringTimeInMS);

                // check if processes are executable
                if (profile.Processes != null)
                {
                    profile.Processes =
                        profile.Processes.Where(
                            it => !string.IsNullOrWhiteSpace(it.ExecutablePath) &&
                            (File.Exists(it.ExecutablePath) || ExistsInPath(it.ExecutablePath)))
                            .ToList();
                }

                if (profile.Processes == null || !profile.Processes.Any())
                {
                    EventLog.WriteEntry("No valid processes defined. Loading defaults", EventLogEntryType.Warning);
                    profile.Processes =
                        new List<ProcessInfo>(SettingsJson.DefaultSettings.MonitoringProfiles.First().Processes);
                }
            }
            // EventLog.WriteEntry("Saving back settings after checking", EventLogEntryType.Information);
            // Settings.Instance.Store();
        }


        private bool ExistsInPath(string filepath)
        {
            return Environment.ExpandEnvironmentVariables("%PATH%")
                .Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries)
                .Any(it => File.Exists(Path.Combine(it, filepath)));
        }
    }
}