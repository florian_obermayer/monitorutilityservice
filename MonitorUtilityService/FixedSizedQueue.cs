﻿using System.Collections.Concurrent;

namespace MonitorUtility
{
    public class FixedSizedQueue<T> : ConcurrentQueue<T>
    {
        public int Limit { get; set; }

        public new void Enqueue(T obj)
        {
            lock (this)
            {
                base.Enqueue(obj);
                T result;
                while (Count > Limit && TryDequeue(out result)) ;
            }  
        }

        public void Clear()
        {
            lock (this)
            {
                T result;
                while (TryDequeue(out result)) ;
            }
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return $"Items: [{string.Join("|", this)}] , Count: {Count}, Limit: {Limit}";
        }
    }
}