﻿using System.ServiceProcess;
using ServiceProcess.Helpers;

namespace MonitorUtility
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[]
            {
                new MonitorUtility(), 
            };

#if !DEBUG
            ServiceBase.Run(servicesToRun);
#else
            servicesToRun.LoadServices(); //AND HERE
#endif
        }
    }
}
