### Command Line Args ###

first arg: idle time in milliseconds until the shutdown will start
second arg: cpu level threshold (double value in 1 <= x <= 100)
third arg: audiolevel (double value 0 < x < 1